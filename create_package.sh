#!/usr/bin/sh

#Making package dirs
mkdir -p pkg/apps
mkdir -p pkg/data/deskbar/menu/Applications
#Update binary
rm -f pkg/apps/moex_view
cp moex_view pkg/apps/moex_view
chmod +x pkg/apps/moex_view
pushd pkg/data/deskbar/menu/Applications
rm -f moex_view
ln -s -r ../../../../apps/moex_view moex_view
popd

#Create package
pushd pkg
package create -C ./ ../moex_view.hpkg 
popd
