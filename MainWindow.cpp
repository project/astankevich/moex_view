#include "MainWindow.h"

#include <Application.h>
#include <PopUpMenu.h>
#include <MenuField.h>
#include <MenuItem.h>
const int TICKER_CHANGED = 'tkch';
MainWindow::MainWindow(void)
	:	BWindow(BRect(100,100,500,400),"MOEX View",B_TITLED_WINDOW, B_ASYNCHRONOUS_CONTROLS)
{
	BRect r(Bounds());
	
	BPopUpMenu* tickerMenu = new BPopUpMenu("Ticker");
	BMenuField * tickerMenuField = new BMenuField("ticker", "Ticker", tickerMenu);//, false, B_FOLLOW_LEFT_TOP);
	
	BMessage * sberTickerMessage = new BMessage(TICKER_CHANGED);
	sberTickerMessage->AddString("ticker", "SBER");
	BMessage * gazpTickerMessage = new BMessage(TICKER_CHANGED);
	gazpTickerMessage->AddString("ticker", "GAZP");
	BMessage * gmknTickerMessage = new BMessage(TICKER_CHANGED);
	gmknTickerMessage->AddString("ticker", "GMKN");
	BMessage * rosnTickerMessage = new BMessage(TICKER_CHANGED);
	rosnTickerMessage->AddString("ticker", "ROSN");
	tickerMenu->AddItem(new BMenuItem ("SBER", sberTickerMessage));
	tickerMenu->AddItem(new BMenuItem ("GAZP", gazpTickerMessage));
	tickerMenu->AddItem(new BMenuItem ("GMKN", gmknTickerMessage));
	tickerMenu->AddItem(new BMenuItem ("ROSN", rosnTickerMessage));
	AddChild(tickerMenuField);
	
	priceChart = new PriceChart(r);
	AddChild(priceChart);
	priceChart->SetViewColor(ui_color(B_PANEL_BACKGROUND_COLOR));
	
}

void MainWindow::requestPrice(BString ticker, BString interval, int start) {
	std::cout << "Prepare request "<< "\n";
	//BString ticker("SBER");
	if (start==0) {
		SetTitle(BString("MOEX View ").Append(ticker));
		priceChart->ClearPoints();
		fTicker = ticker;
		fInterval = interval;
	}
	BString URLString;
	URLString.SetToFormat("http://iss.moex.com/iss/history/engines/stock/markets/shares/boards/TQBR/securities/%s.csv?iss.meta=off&iss.only=marketdata&marketdata.columns=OPEN,CLOSE,HIGH,LOW&from=2024-06-01&interval=%s&start=%d", ticker.String(), interval.String(), start);
	std::cout <<URLString << "\n"; 
	
	auto url = BUrl(URLString);
	
	if (url.IsValid())
	{
		auto request = BHttpRequest(url);
		
		fResult = fSession.Execute(std::move(request), nullptr, BMessenger(this));
		std::cout << "Request sent "<< "\n";
		
	}
}

void
MainWindow::MessageReceived(BMessage *msg)
{

	switch (msg->what)
	{
		using namespace BPrivate::Network;
		case UrlEvent::RequestCompleted:
		{
			auto id = msg->GetInt32(UrlEventData::Id, -1);
			std::cout << "Recieved msg with Id "<<id << "\n";
			if (id == fResult->Identity()) {
				auto success = msg->GetBool(UrlEventData::Success, false);
				if (success && fResult->HasBody()) {
					auto body = fResult->Body();
					//if (body) {
						std::cout << body.text.value() << "\n";
						BStringList lines;
						
						if (body.text.value().Split("\n", true, lines))
						{
							std::cout << "Splitted to lines "<< lines.CountStrings()<<"\n";
							
							int cursorStart=0;
							int cursorTotal=0;
							int cursorPageSize = 100;
							bool allDataRecived=false;
							BStringList cursorCols;
							BString cursorLine = lines.StringAt(lines.CountStrings()-1); //get cursor data
							if (cursorLine.Split(";", false, cursorCols)) {
								const int startColInd = 0;
								const int totalColInd = 1;
								const int pageSizeColInd = 2;
								BString startStr  = cursorCols.StringAt(startColInd);
								BString totalStr  = cursorCols.StringAt(totalColInd);
								BString pageSizeStr  = cursorCols.StringAt(pageSizeColInd);
								if (startStr.ScanWithFormat("%d", &cursorStart) && 
								    totalStr.ScanWithFormat("%d", &cursorTotal ) &&
								    pageSizeStr.ScanWithFormat("%d", &cursorPageSize ) ) {
									if (cursorStart<cursorTotal-1) {
										cursorStart += cursorPageSize;
										requestPrice(fTicker, fInterval, cursorStart);
									}
									else
										allDataRecived = true;
								}
							}
							
							lines.Remove(0, 1); //remove history title
							lines.Remove(lines.CountStrings()-2, 2); //remove cursor
							
							
							const int dateColInd = 1;
							const int openPriceColInd = 6;
							const int highPriceColInd = 8;
							const int lowPriceColInd = 7;
							const int closePriceColInd = 11;
							for (int i=0; i<lines.CountStrings(); i++) {
								BStringList cols;
								BString line = lines.StringAt(i);
								if (line.Split(";", false, cols)) {
									BString datetime = cols.StringAt(dateColInd);
									BString openPriceStr  = cols.StringAt(openPriceColInd);
									BString highPriceStr  = cols.StringAt(highPriceColInd);
									BString lowPriceStr   = cols.StringAt(lowPriceColInd);
									BString closePriceStr = cols.StringAt(closePriceColInd);
									float openPrice;
									float highPrice;
									float lowPrice;
									float closePrice;
									if (openPriceStr.ScanWithFormat("%f", &openPrice) &&
									    highPriceStr.ScanWithFormat("%f", &highPrice) &&
									    lowPriceStr.ScanWithFormat("%f", &lowPrice)   &&
									    closePriceStr.ScanWithFormat("%f", &closePrice))
									{
										std::cout << datetime <<" "<<openPrice<<" "<<highPrice <<" "<<lowPrice<<" "<<closePrice<<"\n";
										priceChart->AddPoint(datetime, openPrice, highPrice, lowPrice, closePrice);
									}
								}
							}
							if (allDataRecived)
								priceChart->Invalidate();
						}
					//}
				}
			}
			break;
		}
		case TICKER_CHANGED: {
			std::cout <<"Ticker Changed!!!\n";
			
			const char * newTicker;
			if (msg->FindString ("ticker", 0, &newTicker)== B_OK) {
				std::cout <<"New ticker"<<newTicker<<"\n";
				requestPrice(BString(newTicker), fInterval, 0);
			}
		}
		default:
		{
			BWindow::MessageReceived(msg);
			break;
		}
	}
}


bool
MainWindow::QuitRequested(void)
{
	be_app->PostMessage(B_QUIT_REQUESTED);
	return true;
}
