#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <cassert>
#include <cstdint>
#include <iostream>

#include <Application.h>
#include <HttpRequest.h>
#include <HttpResult.h>
#include <HttpSession.h>
#include <NetServicesDefs.h>
#include <SupportDefs.h>
#include <Url.h>
#include <Window.h>
#include "PriceChart.h"

using BPrivate::Network::BHttpRequest;
using BPrivate::Network::BHttpSession;
using BPrivate::Network::BHttpResult;

class MainWindow : public BWindow
{
public:
						MainWindow(void);
	void		MessageReceived(BMessage *msg);
	bool		QuitRequested(void);
	void requestPrice(BString ticker, BString interval, int start=0);
			
private:
	BHttpSession fSession;
	std::optional<BHttpResult> fResult;
	
	BString fTicker;
	BString fInterval;
	PriceChart * priceChart;
};

#endif
