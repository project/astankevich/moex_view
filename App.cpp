#include "App.h"



App::App(void)
	:	BApplication("application/x-vnd.dw-TestApp")
{
	ticker = BString("SBER");
	interval= BString("24"); //1 day
	mainwin = new MainWindow();

	
	mainwin->Show();
}

void App::MessageReceived(BMessage *msg)
	{
		switch (msg->what) {
			
		}
		BApplication::MessageReceived(msg);
	}

void App::ArgvReceived(int32 argc, char** argv) {
	std::cout << "Arg count "<< argc <<"\n";
	for (int i=0; i<argc; i++){
		std::cout << "Arg "<< i <<" "<<argv[i]<<"\n";
	}
	if (argc>1) {
		ticker = BString(argv[1]);
		if (argc>2) {
			interval=BString(argv[2]);	
		}
	}
}

void App::ReadyToRun(){
	std::cout << "Ready to run \n";
	mainwin->requestPrice(ticker, interval, 0);
}

int
main(void)
{
	App *app = new App();
	app->Run();
	delete app;
	return 0;
}
