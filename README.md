# Description
MOEX View is share price view client for Moskow exchange (www.moex.com).

![Screenshot](screenshots/screenshot1.jpeg)

# USAGE
From command line:
moex_view TICKER

# Compiling
Open moex_view.pld with Paladin. Choose Build->Make project.

