#ifndef APP_H
#define APP_H

#include <cassert>
#include <cstdint>
#include <iostream>

#include <Application.h>
//#include <HttpRequest.h>
//#include <HttpResult.h>
//#include <HttpSession.h>
#include <NetServicesDefs.h>
#include <SupportDefs.h>
#include <Url.h>
#include "MainWindow.h"
//#include <Expected.h>

//using BPrivate::Network::BHttpRequest;
//using BPrivate::Network::BHttpSession;
//using BPrivate::Network::BHttpResult;

class App : public BApplication
{
public:
	App(void);
	void MessageReceived(BMessage *msg);
	void ArgvReceived(int32 argc, char** argv);
	void ReadyToRun();
private:
	MainWindow *mainwin;
	BString ticker;
	BString interval;
	//BHttpSession fSession;
	//std::optional<BHttpResult> fResult;
};

#endif
