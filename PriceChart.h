/*
 * Copyright 2024, Anton Stankevich <ashc@mail.ru>
 * All rights reserved. Distributed under the terms of the MIT license.
 */
#ifndef PRICECHART_H
#define PRICECHART_H_H


#include <View.h>
#include <Screen.h>


class PriceChart : public BView
{
public:
	enum CurveMode { cmLine, cmCandles, cmBars};
	void ClearPoints();
	void AddPoint(BString datetime, float open, float high, float low, float close);
	
	PriceChart(BRect frame);
	void Draw(BRect updateRect);
	void MouseMoved(BPoint point,uint32 transit, const BMessage* message);
	void MouseDown(BPoint point);
private:
	BString * time;
	float * openPrices;
	float * highPrices;
	float * lowPrices;
	float * closePrices;
	
	float priceMax;
	float priceMin;
	int pointCount;
	int reservedPointCount;
	BPoint mousePoint;
	float horizontalCursorPrice;
	CurveMode curveMode;
	BRect chartRect;
};


#endif // PRICECHART_H_H
