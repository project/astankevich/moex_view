/*
 * Copyright 2024, Anton Stankevich <ashc@mail.ru>
 * All rights reserved. Distributed under the terms of the MIT license.
 */


#include "PriceChart.h"
#include <iostream>
#include <cmath>

const int reservePointStep =200;

PriceChart::PriceChart(BRect frame) :
 BView (frame, "priceChart", B_FOLLOW_ALL_SIDES, B_WILL_DRAW | B_FULL_UPDATE_ON_RESIZE), 
 priceMax(NAN), priceMin(NAN), pointCount(0), reservedPointCount(0),
 mousePoint(100.0f, 100.0f), horizontalCursorPrice(NAN), 
 curveMode(cmCandles), chartRect(frame.left, frame.top, frame.right, frame.bottom) {
 	
 	reservedPointCount += reservePointStep;

 	openPrices  = new float [reservedPointCount];
 	highPrices  = new float [reservedPointCount];
 	lowPrices   = new float [reservedPointCount];
 	closePrices = new float [reservedPointCount];
 	
 	time = new BString [reservedPointCount];
 }
void PriceChart::Draw(BRect updateRect){
	std::cout << "Start update chart \n";
	if (pointCount > 0) {
		rgb_color color={0, 0, 0};
		
		BRect wholeChartRect(Bounds());
		/*BRect*/ chartRect.Set(wholeChartRect.left, wholeChartRect.top, 
		                        wholeChartRect.right, wholeChartRect.bottom);
		chartRect.InsetBy(40, 20);
		//chartRect.OffsetBy(-20, -10);
		//chartRect.InsetBy(40,0);
		SetHighColor(color);
		float step   = chartRect.Width() /(pointCount);
		float stepDiv2 = step / 2.0f;
		float scaleY = chartRect.Height()/(priceMax-priceMin);
		float y0 = chartRect.bottom + scaleY *priceMin;
		float lastXLabelPos = 0.0f;
		BString lastXLabel;
		switch (curveMode) {
			case cmLine: {
				float * points = closePrices;
				BeginLineArray ((pointCount-1) );
					float prev = y0- scaleY*points[0];
					float x=chartRect.left;
					float y;
					for (int i=1; i<pointCount; i++){
						y= y0- scaleY*points[i];
						AddLine (BPoint(x, prev),   BPoint(x+step, y), color);
						prev = y;
						x+= step;
						if ((x > wholeChartRect.right-70) && (lastXLabelPos == 0.0f)) {
							
							lastXLabelPos = x;
							lastXLabel = time[i];
							//std::cout <<"Last label "<< lastXLabel<<" at "<<lastXLabelPos<< "\n";
						}
					}
					
				EndLineArray();
				break;
			}
			case cmCandles: 
			case cmBars: {
					float x=chartRect.left;
					float y_open;
					float y_high;
					float y_low;
					float y_close;
					float topCandleBound;
					float bottomCandleBound;
					//rgb_color risingColor =  {0, 255, 0};
					//rgb_color fallingColor = {255, 0, 0};
					int lineCount = 2 * pointCount;
					BeginLineArray (lineCount);
					for (int i=0; i<pointCount; i++){
						y_open  = y0- scaleY*openPrices[i];
						y_high  = y0- scaleY*highPrices[i];
						y_low   = y0- scaleY*lowPrices[i];
						y_close = y0- scaleY*closePrices[i];
						
						if (y_open > y_close) {
							StrokeRect(BRect(x+1, y_open, x+ step -1, y_close)); //rising
							topCandleBound = y_close;
							bottomCandleBound = y_open;
						} else {
							FillRect(BRect(x+1, y_open, x+ step -1, y_close)); //falling;
							topCandleBound = y_open;
							bottomCandleBound = y_close;
						}
						AddLine (BPoint(x + stepDiv2, y_high), BPoint(x+stepDiv2, topCandleBound), color);
						AddLine (BPoint(x + stepDiv2, y_low),  BPoint(x+stepDiv2, bottomCandleBound), color);
						x+= step;
						if ((x > wholeChartRect.right-70) && (lastXLabelPos == 0.0f)) {
							
							lastXLabelPos = x;
							lastXLabel = time[i];
							//std::cout <<"Last label "<< lastXLabel<<" at "<<lastXLabelPos<< "\n";
						}
					}
						
						
					EndLineArray();
					
				break;
			}
		}
		if (horizontalCursorPrice ==NAN)
			BeginLineArray (2);
		else {
			BeginLineArray (3);
			float horizontalCursorY = y0- scaleY*horizontalCursorPrice;
			AddLine (BPoint(chartRect.left, horizontalCursorY),
						BPoint(chartRect.right, horizontalCursorY), color);
			DrawString(BString().SetToFormat("%3.0f", horizontalCursorPrice), BPoint(3, horizontalCursorY));
		}
			//Draw mouse location
			int pointAtMouse = floor((mousePoint.x - chartRect.left)/step);
			float mousePointXRounded =  chartRect.left + pointAtMouse * step;
			AddLine (BPoint(mousePointXRounded, chartRect.top),
			         BPoint(mousePointXRounded, chartRect.bottom), color);
			AddLine (BPoint(chartRect.left, mousePoint.y), 
					 BPoint(chartRect.right, mousePoint.y), color);
		EndLineArray ();
		float mouseYLabelValue = ( y0 -mousePoint.y) / scaleY;		 
		//std::cout <<y << "\n";
		DrawString(BString().SetToFormat("%3.0f", mouseYLabelValue), BPoint(3, mousePoint.y));
		
		if ((pointAtMouse>=0) && (pointAtMouse< pointCount))
			DrawString(time[pointAtMouse], BPoint(mousePointXRounded, 13));
			
		//Draw number labels
		const int minLabelStep =10;
		float roundedMax = floor(priceMax/10)*10;
		float roundedMin = ceil(priceMin/10)*10;
		
		int yLabelCount = ceil((roundedMax - roundedMin)/minLabelStep);
		float labelValue=roundedMin;
		
		//std::cout <<yLabelCount << "\n";
		
		for  (int i=0; i<=yLabelCount; i++) {
			float y=y0- scaleY*labelValue;
			//std::cout <<y << "\n";
			DrawString(BString().SetToFormat("%3.0f", labelValue), BPoint(chartRect.right, y));
			labelValue += 10;
		}
		if (pointCount>=2) {
			DrawString(time[0], BPoint(chartRect.left, chartRect.bottom+15));
		

			DrawString(lastXLabel, BPoint(lastXLabelPos/*chartRect.right-150*/, chartRect.bottom+15));
		}
		Sync();
	}
	std::cout << "Chart updated! \n";
}

void PriceChart::MouseMoved(BPoint point,uint32 transit, const BMessage* message){
	//if (transit == B_INSIDE_VIEW) {
		std::cout <<"Mouse move at "<<point.x <<" "<<point.y<<"\n";
		mousePoint.Set(point.x, point.y);
		Invalidate(Bounds());
		//Draw(Bounds());
	//}
}

void PriceChart::MouseDown(BPoint point) {
	mousePoint == point;
	if (priceMax != priceMin) {
		float scaleY = chartRect.Height()/(priceMax-priceMin);
		float y0 = chartRect.bottom + scaleY *priceMin;
		horizontalCursorPrice = ( y0 - point.y) / scaleY;
	}
}

void PriceChart::ClearPoints(){
	
	if (pointCount>0) {
		pointCount = 0;
		priceMax=priceMin =NAN;
	}
		
}
void PriceChart::AddPoint(BString datetime, float open, float high, float low, float close) {
	
	pointCount++;
	//Assume that open and close in [low..high] interval.

	if ((low < priceMin) || isnan(priceMin)) 
		priceMin=low;
	if ((high > priceMax) || isnan(priceMax)) 
		priceMax= high;
	//TODO : detect datetime min max
	if (reservedPointCount >= pointCount ){
		//points[pointCount-1] = point;
		//time[pointCount-1] = datetime;
		std::cout << "Added " <<datetime <<" "<< open <<" "<< high<<" "<< low<<" "<< close <<" at reserved space \n";
	}
	else 
	{
		float * old_openPrices  = openPrices;
		float * old_highPrices  = highPrices;
		float * old_lowPrices   = lowPrices;
		float * old_closePrices = closePrices;
		BString * old_time = this->time;
		int oldReservedPointCount = reservedPointCount;
		reservedPointCount += reservePointStep;
		std::cout << "Reserve new space \n";
		
		openPrices  = new float [reservedPointCount];
 		highPrices  = new float [reservedPointCount];
 		lowPrices   = new float [reservedPointCount];
 		closePrices = new float [reservedPointCount];
		
		time =  new BString [reservedPointCount];
		for (int i=0; i<pointCount-1; i++) {
			openPrices[i]  = old_openPrices[i];
			highPrices[i]  = old_highPrices[i];
			lowPrices[i]   = old_lowPrices[i];
			closePrices[i] = old_closePrices[i];
			time[i]   = old_time[i];
		}
		
		
		
		
		if (oldReservedPointCount > 0) {
			std::cout << "Free old arrays \n";
			delete old_openPrices;
			delete old_highPrices;
			delete old_lowPrices;
			delete old_closePrices;
			
			for (int i=0; i<oldReservedPointCount-1; i++)
				old_time[i].~BString();
			//delete old_time; //WTF???
		}
		std::cout << "Added " << open <<" "<< high<<" "<< low<<" "<< close <<" at new space \n";
	}
	time[pointCount-1] = datetime;
	openPrices[pointCount-1]  = open;
	highPrices[pointCount-1]  = high;
	lowPrices[pointCount-1]   = low;
	closePrices[pointCount-1] = close;
}
